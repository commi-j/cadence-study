package tk.labyrinth.simpleorchestra.red.api;

import java.time.Duration;

public class RedConstants {

	public static final String TASK_LIST = "RED_TL";

	public static final Duration TIMEOUT = Duration.ofSeconds(2);
}
