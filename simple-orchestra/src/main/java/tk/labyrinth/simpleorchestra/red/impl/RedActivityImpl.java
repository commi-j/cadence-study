package tk.labyrinth.simpleorchestra.red.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tk.labyrinth.simpleorchestra.red.api.RedActivity;

import java.util.UUID;

@RequiredArgsConstructor
@Service
public class RedActivityImpl implements RedActivity {

	private final RedStorage storage;

	@Override
	public UUID createRedObject(UUID greenObjectUid) {
		if (greenObjectUid != null) {
			throw new RuntimeException("Don't want");
		}
		UUID uid = UUID.randomUUID();
		storage.getMap().put(uid, greenObjectUid + "_hello");
		return uid;
	}
}
