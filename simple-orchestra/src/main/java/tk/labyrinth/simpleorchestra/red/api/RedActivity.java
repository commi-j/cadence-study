package tk.labyrinth.simpleorchestra.red.api;

import com.uber.cadence.activity.ActivityMethod;

import java.util.UUID;

public interface RedActivity {

	@ActivityMethod
	UUID createRedObject(UUID greenObjectUid);
}
