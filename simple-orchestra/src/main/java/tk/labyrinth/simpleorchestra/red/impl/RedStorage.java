package tk.labyrinth.simpleorchestra.red.impl;

import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
public class RedStorage {

	@Getter
	private final ConcurrentMap<UUID, String> map = new ConcurrentHashMap<>();
}
