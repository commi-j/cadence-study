package tk.labyrinth.simpleorchestra.red.impl;

import com.uber.cadence.worker.Worker;
import com.uber.cadence.worker.WorkerOptions;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import tk.labyrinth.simpleorchestra.Constants;
import tk.labyrinth.simpleorchestra.red.api.RedConstants;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class RedWorker {

	private final RedActivityImpl redActivity;

	@PostConstruct
	private void postConstruct() {
		Worker.Factory factory = new Worker.Factory(
				Constants.VALUE,
				new Worker.FactoryOptions.Builder().build());
		//
		Worker worker = factory.newWorker(
				RedConstants.TASK_LIST,
				new WorkerOptions.Builder().build());
		worker.registerActivitiesImplementations(redActivity);
		//
		factory.start();
	}
}
