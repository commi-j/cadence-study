package tk.labyrinth.simpleorchestra.green.api;

import java.time.Duration;

public class GreenConstants {

	public static final String TASK_LIST = "GREEN_TL";

	public static final Duration TIMEOUT = Duration.ofSeconds(2);
}
