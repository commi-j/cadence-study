package tk.labyrinth.simpleorchestra.green.api;

import com.uber.cadence.activity.ActivityMethod;

import java.util.UUID;

public interface GreenActivity {

	@ActivityMethod
	void compensateCreateGreenObject(UUID greenObjectUid);

	@ActivityMethod
	UUID createGreenObject(String value);
}
