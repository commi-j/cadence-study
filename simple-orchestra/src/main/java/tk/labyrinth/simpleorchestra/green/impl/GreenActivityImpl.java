package tk.labyrinth.simpleorchestra.green.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import tk.labyrinth.simpleorchestra.green.api.GreenActivity;

import java.util.UUID;

@RequiredArgsConstructor
@Service
public class GreenActivityImpl implements GreenActivity {

	private final GreenStorage storage;

	@Override
	public void compensateCreateGreenObject(UUID greenObjectUid) {
		storage.getMap().remove(greenObjectUid);
	}

	@Override
	public UUID createGreenObject(String value) {
		UUID uid = UUID.randomUUID();
		storage.getMap().put(uid, value);
		return uid;
	}
}
