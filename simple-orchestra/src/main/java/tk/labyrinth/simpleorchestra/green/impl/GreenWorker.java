package tk.labyrinth.simpleorchestra.green.impl;

import com.uber.cadence.worker.Worker;
import com.uber.cadence.worker.WorkerOptions;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import tk.labyrinth.simpleorchestra.Constants;
import tk.labyrinth.simpleorchestra.green.api.GreenConstants;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class GreenWorker {

	private final GreenActivityImpl greenActivity;

	@PostConstruct
	private void postConstruct() {
		Worker.Factory factory = new Worker.Factory(
				Constants.VALUE,
				new Worker.FactoryOptions.Builder().build());
		//
		Worker worker = factory.newWorker(
				GreenConstants.TASK_LIST,
				new WorkerOptions.Builder().build());
		//
		worker.registerActivitiesImplementations(greenActivity);
		//
		factory.start();
	}
}
