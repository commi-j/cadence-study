package tk.labyrinth.simpleorchestra.green.impl;

import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Component
public class GreenStorage {

	@Getter
	private final ConcurrentMap<UUID, String> map = new ConcurrentHashMap<>();
}
