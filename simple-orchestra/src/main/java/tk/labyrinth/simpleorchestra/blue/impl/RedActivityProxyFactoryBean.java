package tk.labyrinth.simpleorchestra.blue.impl;

import com.uber.cadence.activity.ActivityOptions;
import com.uber.cadence.workflow.Workflow;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;
import tk.labyrinth.simpleorchestra.red.api.RedActivity;
import tk.labyrinth.simpleorchestra.red.api.RedConstants;

@Component("redStub")
public class RedActivityProxyFactoryBean implements FactoryBean<RedActivity> {

	@Override
	public RedActivity getObject() {
		return Workflow.newActivityStub(
				RedActivity.class,
				new ActivityOptions.Builder()
						.setScheduleToCloseTimeout(RedConstants.TIMEOUT)
						.setTaskList(RedConstants.TASK_LIST)
						.build());
	}

	@Override
	public Class<?> getObjectType() {
		return RedActivity.class;
	}
}
