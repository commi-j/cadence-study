package tk.labyrinth.simpleorchestra.blue.impl;

import com.uber.cadence.client.WorkflowClient;
import com.uber.cadence.client.WorkflowOptions;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import tk.labyrinth.simpleorchestra.Constants;
import tk.labyrinth.simpleorchestra.green.impl.GreenStorage;
import tk.labyrinth.simpleorchestra.red.impl.RedStorage;

import java.time.Duration;
import java.time.Instant;

@Slf4j
@RequiredArgsConstructor
@RestController
public class BlueRestController implements ApplicationRunner {

	private final WorkflowClient client = WorkflowClient.newInstance(Constants.VALUE);

	private final GreenStorage greenStorage;

	private final RedStorage redStorage;

	@PostMapping("process")
	public String process(String value) {
		CreateSmthWorkflow createSmthWorkflow = client.newWorkflowStub(
				CreateSmthWorkflow.class,
				new WorkflowOptions.Builder()
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(2))
						.setTaskList(BlueConstants.TASK_LIST)
						.build());
		//
		Object result;
		Instant started = Instant.now();
		try {
			result = createSmthWorkflow.createSmth(value);
		} catch (RuntimeException ex) {
			log.error("", ex);
			result = ex;
		}
		System.out.println("Dur: " + Duration.between(started, Instant.now()).toMillis());
		//
		return String.valueOf(result);
	}

	@Override
	public void run(ApplicationArguments args) {
		String result = process("some-value");
		//
		System.out.println(result);
	}
}
