package tk.labyrinth.simpleorchestra.blue.impl;

import com.uber.cadence.workflow.WorkflowMethod;

public interface CreateSmthWorkflow {

	@WorkflowMethod(name = "azure-wf")
	CreateSmthResult createSmth(String value);
}
