package tk.labyrinth.simpleorchestra.blue.impl;

import lombok.Builder;
import lombok.Value;

import java.util.UUID;

@Builder(toBuilder = true)
@Value
public class CreateSmthResult {

	UUID greenObjectUid;

	UUID redObjectUid;
}
