package tk.labyrinth.simpleorchestra.blue.impl;

import com.uber.cadence.activity.ActivityOptions;
import com.uber.cadence.workflow.Saga;
import com.uber.cadence.workflow.Workflow;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import tk.labyrinth.simpleorchestra.green.api.GreenActivity;
import tk.labyrinth.simpleorchestra.green.api.GreenConstants;
import tk.labyrinth.simpleorchestra.red.api.RedActivity;

import java.util.UUID;

@Component
@RequiredArgsConstructor
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CreateSmthWorkflowImpl implements CreateSmthWorkflow {

	@Autowired
	@Qualifier("redStub")
	private RedActivity redActivity;

	private GreenActivity createGreenActivityProxy() {
		return Workflow.newActivityStub(
				GreenActivity.class,
				new ActivityOptions.Builder()
						.setScheduleToCloseTimeout(GreenConstants.TIMEOUT)
						.setTaskList(GreenConstants.TASK_LIST)
						.build());
	}

	@Override
	public CreateSmthResult createSmth(String value) {
		CreateSmthResult result;
		Saga saga = new Saga(new Saga.Options.Builder().build());
		try {
			UUID greenObjectUid = createGreenActivityProxy().createGreenObject(value);
			saga.addCompensation(createGreenActivityProxy()::compensateCreateGreenObject, greenObjectUid);
			//
			UUID redObjectUid = redActivity.createRedObject(greenObjectUid);
			//
			result = CreateSmthResult.builder()
					.greenObjectUid(greenObjectUid)
					.redObjectUid(redObjectUid)
					.build();
		} catch (RuntimeException ex) {
			saga.compensate();
			throw ex;
			// result = withFlkReport;
		}
		return result;
	}
}
