package tk.labyrinth.simpleorchestra.blue.impl;

import com.uber.cadence.worker.Worker;
import com.uber.cadence.worker.WorkerOptions;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.stereotype.Component;
import tk.labyrinth.simpleorchestra.Constants;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class BlueWorker {

	private final ObjectProvider<CreateSmthWorkflowImpl> wfProvider;

	@PostConstruct
	private void postConstruct() {
		Worker.Factory factory = new Worker.Factory(
				Constants.VALUE,
				new Worker.FactoryOptions.Builder().build());
		//
		Worker worker = factory.newWorker(
				BlueConstants.TASK_LIST,
				new WorkerOptions.Builder().build());
		//
		worker.addWorkflowImplementationFactory(CreateSmthWorkflow.class, wfProvider::getObject);
		//
		factory.start();
	}
}
