package tk.labyrinth.simpleorchestra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ColourfulApplication {

	public static void main(String... args) {
		SpringApplication.run(ColourfulApplication.class, args);
	}
}
