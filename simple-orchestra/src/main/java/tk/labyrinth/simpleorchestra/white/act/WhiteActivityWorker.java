package tk.labyrinth.simpleorchestra.white.act;

import com.uber.cadence.worker.Worker;
import com.uber.cadence.worker.WorkerOptions;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import tk.labyrinth.simpleorchestra.white.domain.WhiteDomain;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class WhiteActivityWorker {

	private final WhiteActivityImpl whiteActivity;

	@PostConstruct
	private void postConstruct() {
		Worker.Factory factory = new Worker.Factory(
				WhiteDomain.VALUE,
				new Worker.FactoryOptions.Builder().build());
		//
		Worker worker = factory.newWorker(WhiteActivity.TASK_LIST, new WorkerOptions.Builder().build());
		worker.registerActivitiesImplementations(whiteActivity);
		//
		factory.start();
	}
}
