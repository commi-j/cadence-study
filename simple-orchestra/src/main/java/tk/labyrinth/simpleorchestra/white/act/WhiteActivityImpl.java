package tk.labyrinth.simpleorchestra.white.act;

import org.springframework.stereotype.Service;

@Service
public class WhiteActivityImpl implements WhiteActivity {

	@Override
	public String doSmth(String value) {
		return "Hello from ACT: " + value;
	}
}
