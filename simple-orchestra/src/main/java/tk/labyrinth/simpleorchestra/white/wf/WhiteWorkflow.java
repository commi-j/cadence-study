package tk.labyrinth.simpleorchestra.white.wf;

import com.uber.cadence.workflow.WorkflowMethod;

public interface WhiteWorkflow {

	static final String TASK_LIST = "WF_LIST";

	@WorkflowMethod
	String doSmth(String value);
}
