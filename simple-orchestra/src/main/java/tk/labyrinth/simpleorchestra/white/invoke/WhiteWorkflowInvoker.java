package tk.labyrinth.simpleorchestra.white.invoke;

import com.uber.cadence.client.WorkflowClient;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import tk.labyrinth.simpleorchestra.white.domain.WhiteDomain;

@Component
@RequiredArgsConstructor
public class WhiteWorkflowInvoker implements ApplicationRunner {

	private final WorkflowClient client = WorkflowClient.newInstance(WhiteDomain.VALUE);

	@Override
	public void run(ApplicationArguments args) {
//		WhiteWorkflow whiteWorkflow = client.newWorkflowStub(
//				WhiteWorkflow.class,
//				new WorkflowOptions.Builder()
//						.setExecutionStartToCloseTimeout(Duration.ofSeconds(2))
//						.setTaskList(WhiteWorkflow.TASK_LIST)
//						.build());
//		//
//		Object result;
//		try {
//			result = whiteWorkflow.doSmth("Hello from Invoker");
//		} catch (RuntimeException ex) {
//			result = ex;
//		}
//		//
//		System.out.println(result);
	}
}
