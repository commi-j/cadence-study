package tk.labyrinth.simpleorchestra.white.wf;

import com.uber.cadence.worker.Worker;
import com.uber.cadence.worker.WorkerOptions;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.stereotype.Component;
import tk.labyrinth.simpleorchestra.white.domain.WhiteDomain;

import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class WhiteWorkflowWorker {

	private final ObjectProvider<WhiteWorkflowImpl> workflowImplProvider;

	@PostConstruct
	private void postConstruct() {
		Worker.Factory factory = new Worker.Factory(
				WhiteDomain.VALUE,
				new Worker.FactoryOptions.Builder().build());
		//
		Worker worker = factory.newWorker(WhiteWorkflow.TASK_LIST, new WorkerOptions.Builder().build());
		worker.addWorkflowImplementationFactory(WhiteWorkflow.class, workflowImplProvider::getObject);
		//
		factory.start();
	}
}
