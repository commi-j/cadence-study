package tk.labyrinth.simpleorchestra.white.act;

import com.uber.cadence.activity.ActivityMethod;

public interface WhiteActivity {

	static final String TASK_LIST = "ACT_LIST";

	@ActivityMethod
	String doSmth(String value);
}
