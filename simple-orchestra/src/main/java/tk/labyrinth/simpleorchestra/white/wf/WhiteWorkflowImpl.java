package tk.labyrinth.simpleorchestra.white.wf;

import com.uber.cadence.activity.ActivityOptions;
import com.uber.cadence.workflow.Workflow;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import tk.labyrinth.simpleorchestra.white.act.WhiteActivity;

import java.time.Duration;

@Scope(BeanDefinition.SCOPE_PROTOTYPE)
@Component
public class WhiteWorkflowImpl implements WhiteWorkflow {

	@Override
	public String doSmth(String value) {
		WhiteActivity whiteActivity = Workflow.newActivityStub(
				WhiteActivity.class,
				new ActivityOptions.Builder()
						.setScheduleToCloseTimeout(Duration.ofSeconds(2))
						.setTaskList(WhiteActivity.TASK_LIST)
						.build());
		whiteActivity.doSmth("Hi from WF");
		//
		return "Hello from WF: " + value;
	}
}
