# Cadence Study

## Setting Up and Running

### Creating Domain

Must be invoked once before running tests.

```
docker run --rm ubercadence/cli:master --address host.docker.internal:7933 --domain study-domain domain register
```

- https://cadenceworkflow.io/docs/cli/#using-the-cli

## Case Studies

Relevant for:

[![CADENCE-CLIENT](https://img.shields.io/badge/cadence--client-2.7.8-green)](https://mvnrepository.com/artifact/com.uber.cadence/cadence-client/2.7.8)

### How does WF survive app restart?

Hypothesis: WF progress is controlled by Server. If Server has events for WF which are not collected it assumes Worker
is lost and assigns new one.

```
tk.labyrinth.cadence.simpleworker.WorkflowWorkerRestartCase
```

Observed:

- New Worker receives WF in 5 seconds after previous Worker was shut down and ACT completed;
- Old Worker seems to not leave invocation of activity despite 'shutdownNow' call. Probably a resource leak.

### What happens to WF and ACT executions when timeout reaches?

Hypothesis: execution has no awareness of timeout and simply continues.

```
tk.labyrinth.cadence.simpleworker.ExecutionAfterTimeoutCases
```

Observed:

- Workflow - as expected;
- GlobalActivity - as expected;
- LocalActivity:
    - Execution - as expected;
    - Invocation does not receive any timeout signal and waits until execution ends;
        - tk.labyrinth.cadence.simpleworker.ExecutionAfterTimeoutCases.testLocalActivityWithoutRetries

### TODO: Find out if compensations could work unpredictably and whether they should be registered as ACT