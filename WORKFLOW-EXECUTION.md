```mermaid
sequenceDiagram
    participant Cadence Server
```

```mermaid
graph TD;
  A-->B;
  A-->C;
  B-->D;
  C-->D;
```