package tk.labyrinth.cadence.simpleworker;

import org.junit.jupiter.api.Test;
import tk.labyrinth.cadence.misc4j.java.lang.ThreadUtils;
import tk.labyrinth.cadence.simpleworker.test.CadenceStudyTestBase;
import tk.labyrinth.cadence.simpleworker.test.TestWorkerHandler;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class LocalActivityDrowningPollerCases extends CadenceStudyTestBase {

	@Test
//	@Timeout(3)
	void testSmth() throws InterruptedException {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorkerWithCustomOptions(
				workerOptions -> workerOptions
						.setMaxConcurrentLocalActivityExecutionSize(2)
		);
		String taskList = workerHandler.getTaskList();
		//
		AtomicInteger workflowVisitCounter = new AtomicInteger(0);
		AtomicInteger localActivityVisitCounter = new AtomicInteger(0);
		AtomicInteger workflowInvocationFinishedCounter = new AtomicInteger(0);
		//
		UUID localActivityRunnableUid = registerRunnable(() -> {
			int visitCount = localActivityVisitCounter.incrementAndGet();
			//
			ThreadUtils.sleep(40000);
		});
		//
		int number = 20;
		CountDownLatch countDownLatch = new CountDownLatch(number);
		ForkJoinPool.commonPool().execute(() -> {
			for (int i = 0; i < number; i++) {
				ForkJoinPool.commonPool().execute(() -> {
					runWorkflowSync(
							options -> options
									.setExecutionStartToCloseTimeout(Duration.ofSeconds(50))
									.setTaskList(taskList)
									.setTaskStartToCloseTimeout(Duration.ofSeconds(1)),
							() -> {
								workflowVisitCounter.incrementAndGet();
								//
								runLocalActivitySync(
										options -> options,
										localActivityRunnableUid);
								//
							});
					//
					countDownLatch.countDown();
				});
				//
				workflowInvocationFinishedCounter.incrementAndGet();
				ThreadUtils.sleep(100);
			}
		});
		//
//		ThreadUtils.sleep(20000);
		countDownLatch.await(30000, TimeUnit.MILLISECONDS);
		//
		System.out.println(workflowVisitCounter.get());
		System.out.println(workflowInvocationFinishedCounter.get());
		System.out.println(localActivityVisitCounter.get());
//		Assertions.assertEquals(2, workflowVisitCounter.get());
//		Assertions.assertEquals(2, localActivityVisitCounter.get());
	}
}
