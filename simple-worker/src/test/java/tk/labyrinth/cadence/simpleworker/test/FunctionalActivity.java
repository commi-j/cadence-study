package tk.labyrinth.cadence.simpleworker.test;

import com.uber.cadence.activity.ActivityMethod;

import java.util.UUID;

public interface FunctionalActivity {

	@ActivityMethod
	Object run(UUID uid);
}
