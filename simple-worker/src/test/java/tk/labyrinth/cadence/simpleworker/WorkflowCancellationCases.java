package tk.labyrinth.cadence.simpleworker;

import com.uber.cadence.client.WorkflowFailureException;
import com.uber.cadence.workflow.ExternalWorkflowStub;
import com.uber.cadence.workflow.Workflow;
import io.vavr.control.Try;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.cadence.misc4j.java.lang.ThreadUtils;
import tk.labyrinth.cadence.simpleworker.test.CadenceStudyTestBase;
import tk.labyrinth.cadence.simpleworker.test.TestWorkerHandler;

import java.time.Duration;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
public class WorkflowCancellationCases extends CadenceStudyTestBase {

	@Test
	void testCaseWorkflowCancelsItself() {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorker();
		//
		AtomicBoolean firstActivityExecuted = new AtomicBoolean(false);
		AtomicBoolean secondActivityExecuted = new AtomicBoolean(false);
		//
		Try<Void> result = runWorkflowSync(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(5))
						.setTaskList(workerHandler.getTaskList()),
				() -> {
					//
					runNonRepeatableGlobalActivitySync(
							options -> options
									.setScheduleToCloseTimeout(Duration.ofSeconds(1)),
							() -> {
								// doNothing();
							});
					//
					System.out.println();
					ExternalWorkflowStub externalWorkflowStub = Workflow.newUntypedExternalWorkflowStub(
							Workflow.getWorkflowInfo().getWorkflowId());
					externalWorkflowStub.cancel();
					//
					//
					runNonRepeatableGlobalActivitySync(
							options -> options
									.setScheduleToCloseTimeout(Duration.ofSeconds(10)),
							() -> {
								// In order to cancel workflow
								//
								firstActivityExecuted.set(true);
							});
					//
					runNonRepeatableGlobalActivitySync(
							options -> options
									.setScheduleToCloseTimeout(Duration.ofSeconds(1)),
							() -> secondActivityExecuted.set(true));
				}
		);
		//
		Assertions.assertTrue(result.isFailure());
		Assertions.assertEquals(WorkflowFailureException.class, result.getCause().getClass());
		Assertions.assertFalse(firstActivityExecuted.get());
		Assertions.assertFalse(secondActivityExecuted.get());
	}

	@Test
	void testCaseWorkflowCancelsNeighbour() throws ExecutionException, InterruptedException {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorker();
		//
		AtomicReference<String> firstWorkflowIdReference = new AtomicReference<>(null);
		//
		CompletableFuture<Try<Void>> firstWorkflowFuture = runWorkflowAsyncByCadence(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(5))
						.setTaskList(workerHandler.getTaskList()),
				() -> {
					firstWorkflowIdReference.set(Workflow.getWorkflowInfo().getWorkflowId());
					//
					runNonRepeatableGlobalActivitySync(
							options -> options
									.setScheduleToCloseTimeout(Duration.ofSeconds(1)),
							() -> ThreadUtils.sleep(500));
					//
					runNonRepeatableGlobalActivitySync(
							options -> options
									.setScheduleToCloseTimeout(Duration.ofSeconds(1)),
							() -> {
								// doNothing();
							});
				});
		//
		Try<Void> secondWorkflowResult = runWorkflowSync(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(5))
						.setTaskList(workerHandler.getTaskList()),
				() -> {
					ThreadUtils.sleep(100);
					//
					ExternalWorkflowStub externalWorkflowStub = Workflow.newUntypedExternalWorkflowStub(
							firstWorkflowIdReference.get());
					externalWorkflowStub.cancel();
					//
					// We have to have some action after cancellation request or it won't be processed.
					runNonRepeatableGlobalActivitySync(
							options -> options
									.setScheduleToCloseTimeout(Duration.ofSeconds(1)),
							() -> {
								// doNothing();
							});
				});
		//
		Try<Void> firstWorkflowResult = firstWorkflowFuture.get();
		//
		Assertions.assertTrue(firstWorkflowResult.isFailure());
		Assertions.assertEquals(CompletionException.class, firstWorkflowResult.getCause().getClass());
		Assertions.assertEquals(CancellationException.class, firstWorkflowResult.getCause().getCause().getClass());
		//
		Assertions.assertTrue(secondWorkflowResult.isSuccess());
	}
}
