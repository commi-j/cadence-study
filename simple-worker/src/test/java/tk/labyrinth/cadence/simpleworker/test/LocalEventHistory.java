package tk.labyrinth.cadence.simpleworker.test;

import lombok.Getter;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

public class LocalEventHistory {

	@Getter
	private final List<String> entries = new CopyOnWriteArrayList<>();

	/**
	 * @param entry non-null
	 *
	 * @return number of entries equal to provided one
	 */
	public int addEntry(String entry) {
		entries.add(entry);
		return (int) entries.stream()
				.filter(entriesElement -> Objects.equals(entriesElement, entry))
				.count();
	}
}
