package tk.labyrinth.cadence.simpleworker;

import com.uber.cadence.client.WorkflowFailureException;
import io.vavr.control.Try;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import tk.labyrinth.cadence.simpleworker.test.CadenceStudyTestBase;
import tk.labyrinth.cadence.simpleworker.test.TestWorkerHandler;
import tk.labyrinth.cadence.tools.FunctionalGsonDataConverter;

import java.time.Duration;
import java.util.UUID;

public class ActivitiesThrowsExceptionsCases extends CadenceStudyTestBase {

	@Test
	@Timeout(2)
	void testActivityThrowsExceptionConvertedToString() {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorker(
				workerOptions -> workerOptions
						.setDataConverter(new FunctionalGsonDataConverter()
								.toDataFunction((superCallback, values) -> {
									byte[] result;
									if (values == null || values.length != 1 || !(values[0] instanceof Throwable)) {
										result = superCallback.apply(values);
									} else {
										result = superCallback.apply(new Object[]{new Object()});
									}
									return result;
								})));
		String taskList = workerHandler.getTaskList();
		//
		UUID localActivityRunnableUid = registerRunnable(() -> {
			throw new RuntimeException();
		});
		//
		Try<Object> tryObject = Try.ofSupplier(() -> callWorkflowSync(
				workflowOptions -> workflowOptions
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(1))
						.setTaskList(taskList),
				() -> {
					return callLocalActivitySync(
							options -> options,
							localActivityRunnableUid);
					//
				}));
		//
		Assertions.assertEquals(Try.success("foo"), tryObject);
	}

	@Test
	@Timeout(3)
	void testGlobalActivityThrowsExceptionConvertedToNull() {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorker(
				workerOptions -> workerOptions
						.setDataConverter(new FunctionalGsonDataConverter()
								.toDataFunction((superCallback, values) -> {
									byte[] result;
									if (values == null || values.length != 1 || !(values[0] instanceof Throwable)) {
										result = superCallback.apply(values);
									} else {
										result = null;
									}
									return result;
								})));
		//
		Try<Object> tryObject = Try.ofSupplier(() -> callWorkflowSync(
				workflowOptions -> workflowOptions
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(2))
						.setTaskList(workerHandler.getTaskList()),
				() -> {
					return callGlobalActivitySync(
							options -> options,
							() -> {
								throw new RuntimeException();
							});
					//
				}));
		//
		Assertions.assertEquals(WorkflowFailureException.class, tryObject.getCause().getClass());
		Assertions.assertNull(tryObject.getCause().getCause());
	}

	@Test
	@Timeout(2)
	void testLocalActivityThrowsExceptionConvertedToNull() {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorker(
				workerOptions -> workerOptions
						.setDataConverter(new FunctionalGsonDataConverter()
								.toDataFunction((superCallback, values) -> {
									byte[] result;
									if (values == null || values.length != 1 || !(values[0] instanceof Throwable)) {
										result = superCallback.apply(values);
									} else {
										result = null;
									}
									return result;
								})));
		//
		Try<Object> tryObject = Try.ofSupplier(() -> callWorkflowSync(
				workflowOptions -> workflowOptions
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(1))
						.setTaskList(workerHandler.getTaskList()),
				() -> {
					return callLocalActivitySync(
							options -> options,
							() -> {
								throw new RuntimeException();
							});
					//
				}));
		//
		Assertions.assertEquals(Try.success(null), tryObject);
	}
}
