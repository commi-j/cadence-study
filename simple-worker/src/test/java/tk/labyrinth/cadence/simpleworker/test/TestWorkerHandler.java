package tk.labyrinth.cadence.simpleworker.test;

import com.uber.cadence.worker.Worker;
import com.uber.cadence.worker.WorkerOptions;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.UUID;
import java.util.function.UnaryOperator;

@RequiredArgsConstructor
public class TestWorkerHandler {

	@Getter
	private final Worker.Factory factory;

	@Getter
	private final String taskList;

	public void shutdown() {
		factory.shutdown();
	}

	public void shutdownNow() {
		factory.shutdownNow();
	}

	public static TestWorkerHandler startNewFunctionalActivityWorker() {
		Worker.Factory factory = new Worker.Factory("study-domain");
		String taskList = UUID.randomUUID().toString();
		Worker worker = factory.newWorker(taskList);
		worker.registerActivitiesImplementations(new FunctionalActivityImpl());
		factory.start();
		return new TestWorkerHandler(factory, taskList);
	}

	public static TestWorkerHandler startNewFunctionalActivityWorker(String taskList) {
		Worker.Factory factory = new Worker.Factory("study-domain");
		Worker worker = factory.newWorker(taskList);
		worker.registerActivitiesImplementations(new FunctionalActivityImpl());
		factory.start();
		return new TestWorkerHandler(factory, taskList);
	}

	public static TestWorkerHandler startNewFunctionalWorker() {
		return startNewFunctionalWorker(UUID.randomUUID().toString(), UnaryOperator.identity(), UnaryOperator.identity());
	}

	public static TestWorkerHandler startNewFunctionalWorker(String taskList) {
		return startNewFunctionalWorker(taskList, UnaryOperator.identity(), UnaryOperator.identity());
	}

	public static TestWorkerHandler startNewFunctionalWorker(
			String taskList,
			UnaryOperator<Worker.FactoryOptions.Builder> factoryOptionsConfigurer,
			UnaryOperator<WorkerOptions.Builder> workerOptionsConfigurer) {
		Worker.Factory factory = new Worker.Factory(
				"study-domain",
				factoryOptionsConfigurer.apply(new Worker.FactoryOptions.Builder()).build());
		Worker worker = factory.newWorker(
				taskList,
				workerOptionsConfigurer.apply(new WorkerOptions.Builder()).build());
		worker.registerActivitiesImplementations(new FunctionalActivityImpl());
		worker.registerWorkflowImplementationTypes(FunctionalWorkflowImpl.class);
		factory.start();
		return new TestWorkerHandler(factory, taskList);
	}

	public static TestWorkerHandler startNewFunctionalWorker(
			UnaryOperator<WorkerOptions.Builder> workerOptionsConfigurer) {
		return startNewFunctionalWorker(UUID.randomUUID().toString(), UnaryOperator.identity(), workerOptionsConfigurer);
	}

	public static TestWorkerHandler startNewFunctionalWorkerWithCustomFactoryOptions(
			UnaryOperator<Worker.FactoryOptions.Builder> factoryOptionsConfigurer) {
		return startNewFunctionalWorker(UUID.randomUUID().toString(), factoryOptionsConfigurer, UnaryOperator.identity());
	}

	public static TestWorkerHandler startNewFunctionalWorkerWithCustomOptions(
			UnaryOperator<WorkerOptions.Builder> workerOptionsConfigurer) {
		return startNewFunctionalWorker(UUID.randomUUID().toString(), UnaryOperator.identity(), workerOptionsConfigurer);
	}

	public static TestWorkerHandler startNewFunctionalWorkflowWorker() {
		Worker.Factory factory = new Worker.Factory("study-domain");
		String taskList = UUID.randomUUID().toString();
		Worker worker = factory.newWorker(taskList);
		worker.registerWorkflowImplementationTypes(FunctionalWorkflowImpl.class);
		factory.start();
		return new TestWorkerHandler(factory, taskList);
	}

	public static TestWorkerHandler startNewFunctionalWorkflowWorker(String taskList) {
		Worker.Factory factory = new Worker.Factory("study-domain");
		Worker worker = factory.newWorker(taskList);
		worker.registerWorkflowImplementationTypes(FunctionalWorkflowImpl.class);
		factory.start();
		return new TestWorkerHandler(factory, taskList);
	}

	public static TestWorkerHandler startWithActivities(Object... activities) {
		Worker.Factory factory = new Worker.Factory("study-domain");
		String taskList = UUID.randomUUID().toString();
		Worker worker = factory.newWorker(taskList);
		worker.registerActivitiesImplementations(activities);
		factory.start();
		return new TestWorkerHandler(factory, taskList);
	}

	public static TestWorkerHandler startWithWorkflowTypes(Class<?>... workflowTypes) {
		Worker.Factory factory = new Worker.Factory("study-domain");
		String taskList = UUID.randomUUID().toString();
		Worker worker = factory.newWorker(taskList);
		worker.registerWorkflowImplementationTypes(workflowTypes);
		factory.start();
		return new TestWorkerHandler(factory, taskList);
	}
}
