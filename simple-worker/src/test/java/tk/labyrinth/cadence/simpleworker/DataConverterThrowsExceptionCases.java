package tk.labyrinth.cadence.simpleworker;

import org.junit.jupiter.api.Test;
import tk.labyrinth.cadence.misc4j.java.lang.ThreadUtils;
import tk.labyrinth.cadence.simpleworker.test.CadenceStudyTestBase;
import tk.labyrinth.cadence.simpleworker.test.TestWorkerHandler;
import tk.labyrinth.cadence.tools.FunctionalGsonDataConverter;

import java.time.Duration;

class DataConverterThrowsExceptionCases extends CadenceStudyTestBase {

	@Test
	void testWat() {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorker(
				workerOptions -> workerOptions
						.setDataConverter(new FunctionalGsonDataConverter()
								.toDataFunction((superCallback, values) -> {
									if (values.length == 1 && values[0] instanceof Throwable) {
										throw new RuntimeException("dataConversion");
									}
									return superCallback.apply(values);
								})));
		//
		runWorkflowSync(
				workflowOptions -> workflowOptions
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(15))
						.setTaskStartToCloseTimeout(Duration.ofSeconds(2))
						.setTaskList(workerHandler.getTaskList()),
				() -> {
					System.out.println("STARTED");
					runLocalActivitySync(
							localActivityOptions -> localActivityOptions,
							() -> {
								throw new RuntimeException("localActivity");
							}
					);
				}
		);
	}

	@Test
	void testWat2() {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorker(
				workerOptions -> workerOptions
						.setDataConverter(new FunctionalGsonDataConverter()
								.toDataFunction((superCallback, values) -> {
									if (values.length == 1 && values[0] instanceof Throwable) {
										throw new RuntimeException("dataConversion");
									}
									return superCallback.apply(values);
								})));
		//
		runWorkflowSync(
				workflowOptions -> workflowOptions
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(15))
						.setTaskStartToCloseTimeout(Duration.ofSeconds(2))
						.setTaskList(workerHandler.getTaskList()),
				() -> {
					System.out.println("STARTED");
					runNonRepeatableLocalActivitySync(
							localActivityOptions -> localActivityOptions,
							() -> {
								ThreadUtils.sleep(7000);
							}
					);
				}
		);
	}
}
