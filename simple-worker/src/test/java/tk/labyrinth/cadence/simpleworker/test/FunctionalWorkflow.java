package tk.labyrinth.cadence.simpleworker.test;

import com.uber.cadence.workflow.WorkflowMethod;

import java.util.UUID;

public interface FunctionalWorkflow {

	@WorkflowMethod
	Object run(UUID uid);
}
