package tk.labyrinth.cadence.simpleworker.test;

import com.uber.cadence.activity.ActivityOptions;
import com.uber.cadence.activity.LocalActivityOptions;
import com.uber.cadence.client.WorkflowClient;
import com.uber.cadence.client.WorkflowOptions;
import com.uber.cadence.workflow.Workflow;
import io.vavr.control.Try;
import lombok.Getter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;

public class CadenceStudyTestBase {

	@Getter
	private final WorkflowClient client = WorkflowClient.newInstance("study-domain");

	private String testMethodName = null;

	@AfterEach
	private void afterEach() {
		testMethodName = null;
	}

	@BeforeEach
	private void beforeEach(TestInfo testInfo) {
		testMethodName = testInfo.getDisplayName();
	}

	protected <T> T callGlobalActivitySync(
			UnaryOperator<ActivityOptions.Builder> optionsConfigurer,
			Supplier<?> supplier) {
		return callGlobalActivitySync(optionsConfigurer, ExecutionFunctionRegistry.putSupplier(supplier));
	}

	@SuppressWarnings("unchecked")
	protected <T> T callGlobalActivitySync(
			UnaryOperator<ActivityOptions.Builder> optionsConfigurer,
			UUID callableUid) {
		return (T) getGlobalActivity(optionsConfigurer).run(callableUid);
	}

	protected <T> T callLocalActivitySync(
			UnaryOperator<LocalActivityOptions.Builder> optionsConfigurer,
			Supplier<?> supplier) {
		return callLocalActivitySync(optionsConfigurer, ExecutionFunctionRegistry.putSupplier(supplier));
	}

	@SuppressWarnings("unchecked")
	protected <T> T callLocalActivitySync(
			UnaryOperator<LocalActivityOptions.Builder> optionsConfigurer,
			UUID callableUid) {
		return (T) getLocalActivity(optionsConfigurer).run(callableUid);
	}

	@SuppressWarnings("unchecked")
	protected <T> T callWorkflowSync(
			UnaryOperator<WorkflowOptions.Builder> optionsConfigurer,
			Supplier<?> supplier) {
		FunctionalWorkflow workflow = newWorkflow(optionsConfigurer);
		return (T) workflow.run(ExecutionFunctionRegistry.putSupplier(supplier));
	}

	protected FunctionalActivity getGlobalActivity(UnaryOperator<ActivityOptions.Builder> optionsConfigurer) {
		return Workflow.newActivityStub(
				FunctionalActivity.class,
				optionsConfigurer.apply(new ActivityOptions.Builder()).build());
	}

	protected FunctionalActivity getLocalActivity(UnaryOperator<LocalActivityOptions.Builder> optionsConfigurer) {
		return Workflow.newLocalActivityStub(
				FunctionalActivity.class,
				optionsConfigurer.apply(new LocalActivityOptions.Builder()).build());
	}

	protected FunctionalWorkflow newWorkflow(UnaryOperator<WorkflowOptions.Builder> optionsConfigurer) {
		return client.newWorkflowStub(
				FunctionalWorkflow.class,
				optionsConfigurer.apply(new WorkflowOptions.Builder().setWorkflowId(testMethodName + "-" + UUID.randomUUID())).build());
	}

	protected UUID registerRunnable(Runnable runnable) {
		return ExecutionFunctionRegistry.putRunnable(runnable);
	}

	protected UUID registerSupplier(Supplier<?> supplier) {
		return ExecutionFunctionRegistry.putSupplier(supplier);
	}

	protected void runGlobalActivitySync(
			UnaryOperator<ActivityOptions.Builder> optionsConfigurer,
			UUID runnableUid) {
		getGlobalActivity(optionsConfigurer).run(runnableUid);
	}

	@Deprecated
	protected void runLocalActivitySync(
			UnaryOperator<LocalActivityOptions.Builder> optionsConfigurer,
			UUID runnableUid) {
		getLocalActivity(optionsConfigurer).run(runnableUid);
	}

	protected void runLocalActivitySync(
			UnaryOperator<LocalActivityOptions.Builder> optionsConfigurer,
			Runnable runnable) {
		tryRunLocalActivitySync(optionsConfigurer, runnable).get();
	}

	/**
	 * Unique activities are not suitable for repeatable invocations
	 * as they will register new runnableUid each time.
	 *
	 * @param optionsConfigurer non-null
	 * @param runnable          non-null
	 */
	protected void runNonRepeatableGlobalActivitySync(
			UnaryOperator<ActivityOptions.Builder> optionsConfigurer,
			Runnable runnable) {
		getGlobalActivity(optionsConfigurer).run(ExecutionFunctionRegistry.putRunnable(runnable));
	}

	/**
	 * Unique activities are not suitable for repeatable invocations
	 * as they will register new runnableUid each time.
	 *
	 * @param optionsConfigurer non-null
	 * @param runnable          non-null
	 */
	protected void runNonRepeatableLocalActivitySync(
			UnaryOperator<LocalActivityOptions.Builder> optionsConfigurer,
			Runnable runnable) {
		getLocalActivity(optionsConfigurer).run(ExecutionFunctionRegistry.putRunnable(runnable));
	}

	protected CompletableFuture<Try<Void>> runWorkflowAsyncByCadence(
			UnaryOperator<WorkflowOptions.Builder> optionsConfigurer,
			Runnable runnable) {
		FunctionalWorkflow workflow = newWorkflow(optionsConfigurer);
		UUID runnableUid = ExecutionFunctionRegistry.putRunnable(runnable);
		return WorkflowClient.execute(workflow::run, runnableUid)
				.handle((value, fault) -> fault == null ? Try.success(null) : Try.failure(fault));
	}

	protected CompletableFuture<Try<Void>> runWorkflowAsyncByFjp(
			UnaryOperator<WorkflowOptions.Builder> optionsConfigurer,
			Runnable runnable) {
		return CompletableFuture.supplyAsync(
				() -> {
					FunctionalWorkflow workflow = newWorkflow(optionsConfigurer);
					UUID runnableUid = ExecutionFunctionRegistry.putRunnable(runnable);
					try {
						workflow.run(runnableUid);
						return Try.success(null);
					} catch (RuntimeException ex) {
						return Try.failure(ex);
					}
				},
				ForkJoinPool.commonPool());
	}

	protected Try<Void> runWorkflowSync(
			UnaryOperator<WorkflowOptions.Builder> optionsConfigurer,
			Runnable runnable) {
		FunctionalWorkflow workflow = newWorkflow(optionsConfigurer);
		UUID runnableUid = ExecutionFunctionRegistry.putRunnable(runnable);
		//
		return Try.of(() -> {
			workflow.run(runnableUid);
			return null;
		});
	}

	protected void runWorkflowSync(
			UnaryOperator<WorkflowOptions.Builder> optionsConfigurer,
			UUID uid) {
		FunctionalWorkflow workflow = newWorkflow(optionsConfigurer);
		workflow.run(uid);
	}

	protected Try<Void> tryRunLocalActivitySync(
			UnaryOperator<LocalActivityOptions.Builder> optionsConfigurer,
			Runnable runnable) {
		FunctionalActivity localActivity = getLocalActivity(optionsConfigurer);
		UUID runnableUid = ExecutionFunctionRegistry.putRunnableWithStackTrace(runnable);
		//
		return Try.ofSupplier(() -> {
			localActivity.run(runnableUid);
			return null;
		});
	}
}
