package tk.labyrinth.cadence.simpleworker;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import tk.labyrinth.cadence.misc4j.java.lang.ThreadUtils;
import tk.labyrinth.cadence.simpleworker.test.CadenceStudyTestBase;
import tk.labyrinth.cadence.simpleworker.test.TestWorkerHandler;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class ChildWorkflowTestFirst extends CadenceStudyTestBase {

	@Test
	@Timeout(3)
	void testLocalActivityLongerThanDecisionTaskTimeout() {
		String workflowTaskList = TestWorkerHandler.startNewFunctionalWorker().getTaskList();
		//
		AtomicInteger workflowVisitCounter = new AtomicInteger(0);
		AtomicInteger localActivityVisitCounter = new AtomicInteger(0);
		//
		UUID localActivityRunnableUid = registerRunnable(() -> {
			int visitCount = localActivityVisitCounter.incrementAndGet();
			//
			if (visitCount == 1) {
				ThreadUtils.sleep(1200);
			}
		});
		//
		runWorkflowSync(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(2))
						.setTaskList(workflowTaskList)
						.setTaskStartToCloseTimeout(Duration.ofSeconds(1))
				,
				() -> {
					workflowVisitCounter.incrementAndGet();
					//
					runLocalActivitySync(
							options -> options,
							localActivityRunnableUid);
				});
		//
		Assertions.assertEquals(2, workflowVisitCounter.get());
		Assertions.assertEquals(2, localActivityVisitCounter.get());
		// TODO: Assert time
	}
}
