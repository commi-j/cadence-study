package tk.labyrinth.cadence.simpleworker;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import tk.labyrinth.cadence.simpleworker.test.CadenceStudyTestBase;
import tk.labyrinth.cadence.simpleworker.test.TestWorkerHandler;

import java.time.Duration;

/**
 * Simplest test to ensure server is up.
 */
public class CadenceServerUpTest extends CadenceStudyTestBase {

	@Test
	void testCadenceServerStarted() {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorker();
		//
		runWorkflowSync(
				options -> options
						.setTaskList(workerHandler.getTaskList())
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(1)),
				() -> {
					// no-op
				});
		//
		Assertions.assertTrue(true);
	}
}
