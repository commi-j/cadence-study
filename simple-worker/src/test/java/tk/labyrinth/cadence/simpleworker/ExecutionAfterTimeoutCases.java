package tk.labyrinth.cadence.simpleworker;

import com.uber.cadence.client.WorkflowTimedOutException;
import com.uber.cadence.common.RetryOptions;
import com.uber.cadence.workflow.ActivityTimeoutException;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import tk.labyrinth.cadence.misc4j.java.util.concurrent.CountDownLatchUtils;
import tk.labyrinth.cadence.misc4j.lib.junit5.ContribAssertions;
import tk.labyrinth.cadence.simpleworker.test.CadenceStudyTestBase;
import tk.labyrinth.cadence.simpleworker.test.TestWorkerHandler;

import java.time.Duration;
import java.time.Instant;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

@Tag("cadence")
public class ExecutionAfterTimeoutCases extends CadenceStudyTestBase {

	private static final String taskList = UUID.randomUUID().toString();

	/**
	 * When GACT reaches timeout WF receives timeout signal, execution continues.
	 */
	@Test
	@Timeout(value = 4)
	void testGlobalActivityWithoutRetries() {
		TestWorkerHandler.startNewFunctionalWorker(taskList);
		//
		AtomicReference<Boolean> activityExecutionEndedNormally = new AtomicReference<>(null);
		AtomicReference<Boolean> activityInvocationEndedExceptionally = new AtomicReference<>(null);
		AtomicReference<Long> activityDuration = new AtomicReference<>(null);
		CountDownLatch countDownLatch = new CountDownLatch(1);
		//
		Instant startedAt = Instant.now();
		runWorkflowSync(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(3))
						.setTaskList(taskList),
				() -> {
					try {
						runNonRepeatableGlobalActivitySync(
								options -> options
										.setScheduleToCloseTimeout(Duration.ofSeconds(1)),
								() -> {
									try {
										Thread.sleep(2000);
										activityExecutionEndedNormally.set(true);
									} catch (InterruptedException ex) {
										activityExecutionEndedNormally.set(false);
									}
									activityDuration.set(Duration.between(startedAt, Instant.now()).toMillis());
									countDownLatch.countDown();
								});
						activityInvocationEndedExceptionally.set(false);
					} catch (ActivityTimeoutException ex) {
						activityInvocationEndedExceptionally.set(true);
					}
				});
		long wfDuration = Duration.between(startedAt, Instant.now()).toMillis();
		//
		CountDownLatchUtils.await(countDownLatch);
		//
		ContribAssertions.assertTrue(activityExecutionEndedNormally.get());
		ContribAssertions.assertTrue(activityInvocationEndedExceptionally.get());
		ContribAssertions.assertWithinRange(1000, 1500, wfDuration);
		ContribAssertions.assertWithinRange(2000, 2500, activityDuration.get());
	}

	/**
	 * Same as {@link #testLocalActivityWithoutRetries()}.
	 */
	@Test
	@Timeout(value = 6)
	void testLocalActivityWithRetries() {
		TestWorkerHandler.startNewFunctionalWorker(taskList);
		//
		AtomicReference<Boolean> activityExecutionEndedNormally = new AtomicReference<>(null);
		AtomicReference<Boolean> activityInvocationEndedExceptionally = new AtomicReference<>(null);
		AtomicReference<Long> activityDuration = new AtomicReference<>(null);
		CountDownLatch countDownLatch = new CountDownLatch(1);
		//
		Instant startedAt = Instant.now();
		runWorkflowSync(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(5))
						.setTaskList(taskList),
				() -> {
					try {
						runNonRepeatableLocalActivitySync(
								options -> options
										.setRetryOptions(new RetryOptions.Builder()
												.setExpiration(Duration.ofSeconds(3))
												.setInitialInterval(Duration.ofSeconds(1))
												.build())
										.setScheduleToCloseTimeout(Duration.ofSeconds(1)),
								() -> {
									try {
										Thread.sleep(4000);
										activityExecutionEndedNormally.set(true);
									} catch (InterruptedException ex) {
										activityExecutionEndedNormally.set(false);
									}
									activityDuration.set(Duration.between(startedAt, Instant.now()).toMillis());
									countDownLatch.countDown();
								});
						activityInvocationEndedExceptionally.set(false);
					} catch (ActivityTimeoutException ex) {
						activityInvocationEndedExceptionally.set(true);
					}
				});
		long wfDuration = Duration.between(startedAt, Instant.now()).toMillis();
		//
		CountDownLatchUtils.await(countDownLatch);
		//
		ContribAssertions.assertTrue(activityExecutionEndedNormally.get());
		ContribAssertions.assertFalse(activityInvocationEndedExceptionally.get());
		ContribAssertions.assertWithinRange(4000, 4500, wfDuration);
		ContribAssertions.assertWithinRange(4000, 4500, activityDuration.get());
	}

	/**
	 * When LACT reaches timeout WF does not receive any signal and simply waits until it ends.
	 */
	@Test
	@Timeout(value = 4)
	void testLocalActivityWithoutRetries() {
		TestWorkerHandler.startNewFunctionalWorker(taskList);
		//
		AtomicReference<Boolean> activityExecutionEndedNormally = new AtomicReference<>(null);
		AtomicReference<Boolean> activityInvocationEndedExceptionally = new AtomicReference<>(null);
		AtomicReference<Long> activityDuration = new AtomicReference<>(null);
		CountDownLatch countDownLatch = new CountDownLatch(1);
		//
		Instant startedAt = Instant.now();
		runWorkflowSync(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(3))
						.setTaskList(taskList),
				() -> {
					try {
						runNonRepeatableLocalActivitySync(
								options -> options
										.setScheduleToCloseTimeout(Duration.ofSeconds(1)),
								() -> {
									try {
										Thread.sleep(2000);
										activityExecutionEndedNormally.set(true);
									} catch (InterruptedException ex) {
										activityExecutionEndedNormally.set(false);
									}
									activityDuration.set(Duration.between(startedAt, Instant.now()).toMillis());
									countDownLatch.countDown();
								});
						activityInvocationEndedExceptionally.set(false);
					} catch (ActivityTimeoutException ex) {
						activityInvocationEndedExceptionally.set(true);
					}
				});
		long wfDuration = Duration.between(startedAt, Instant.now()).toMillis();
		//
		CountDownLatchUtils.await(countDownLatch);
		//
		ContribAssertions.assertTrue(activityExecutionEndedNormally.get());
		ContribAssertions.assertFalse(activityInvocationEndedExceptionally.get());
		ContribAssertions.assertWithinRange(2000, 2500, wfDuration);
		ContribAssertions.assertWithinRange(2000, 2500, activityDuration.get());
	}

	/**
	 * When WF reaches timeout invoker receives timeout signal, execution continues.
	 */
	@Test
	@Timeout(value = 3)
	void testWorkflowWithoutRetries() {
		TestWorkerHandler.startNewFunctionalWorker(taskList);
		//
		AtomicReference<Long> wfExecutionDuration = new AtomicReference<>(null);
		AtomicReference<Boolean> wfExecutionEndedNormally = new AtomicReference<>(null);
		AtomicReference<Long> wfInvocationDuration = new AtomicReference<>(null);
		AtomicReference<Boolean> wfInvocationEndedExceptionally = new AtomicReference<>(null);
		//
		CountDownLatch countDownLatch = new CountDownLatch(1);
		//
		Instant startedAt = Instant.now();
		try {
			runWorkflowSync(
					options -> options
							.setExecutionStartToCloseTimeout(Duration.ofSeconds(1))
							.setTaskList(taskList),
					() -> {
						try {
							Thread.sleep(2000);
							wfExecutionEndedNormally.set(true);
						} catch (InterruptedException ex) {
							wfExecutionEndedNormally.set(false);
						}
						wfExecutionDuration.set(Duration.between(startedAt, Instant.now()).toMillis());
						countDownLatch.countDown();
					});
			wfInvocationEndedExceptionally.set(false);
		} catch (WorkflowTimedOutException ex) {
			wfInvocationEndedExceptionally.set(true);
		}
		wfInvocationDuration.set(Duration.between(startedAt, Instant.now()).toMillis());
		//
		CountDownLatchUtils.await(countDownLatch);
		//
		ContribAssertions.assertTrue(wfExecutionEndedNormally.get());
		ContribAssertions.assertTrue(wfInvocationEndedExceptionally.get());
		ContribAssertions.assertWithinRange(1000, 1500, wfInvocationDuration.get());
		ContribAssertions.assertWithinRange(2000, 2500, wfExecutionDuration.get());
	}
}
