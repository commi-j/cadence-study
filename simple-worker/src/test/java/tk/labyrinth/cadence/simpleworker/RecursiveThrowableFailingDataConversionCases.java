package tk.labyrinth.cadence.simpleworker;

import com.google.gson.Gson;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Test;
import tk.labyrinth.cadence.simpleworker.test.CadenceStudyTestBase;
import tk.labyrinth.cadence.simpleworker.test.TestWorkerHandler;
import tk.labyrinth.cadence.tools.FunctionalGsonDataConverter;

import java.time.Duration;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class RecursiveThrowableFailingDataConversionCases extends CadenceStudyTestBase {

	@Test
	void testLocalActivityThrowsRecursiveException() {
		RecursiveObject recursiveObject0 = new RecursiveObject();
		RecursiveObject recursiveObject1 = new RecursiveObject();
		recursiveObject0.setRecursiveObject(recursiveObject1);
		recursiveObject1.setRecursiveObject(recursiveObject0);
		RecursiveException recex = new RecursiveException(recursiveObject0);
		try {
//			String jacksonString = new ObjectMapper()
//					.configure(SerializationFeature.FAIL_ON_SELF_REFERENCES, false)
//					.configure(SerializationFeature.WRITE_SELF_REFERENCES_AS_NULL, true)
//					.writeValueAsString(recex);
			String gsonString = new Gson().toJson(recex);
			System.out.println();
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		//
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorker(
				workerOptions -> workerOptions
						.setDataConverter(new FunctionalGsonDataConverter()
								.toDataFunction((superCallback, values) -> {
									return null;
								})));
		String taskList = workerHandler.getTaskList();
		//
		AtomicInteger workflowVisitCounter = new AtomicInteger(0);
		AtomicInteger localActivityVisitCounter = new AtomicInteger(0);
		AtomicInteger workflowInvocationFinishedCounter = new AtomicInteger(0);
		//
		UUID localActivityRunnableUid = registerRunnable(() -> {
			throw new RecursiveException(recursiveObject0);
		});
		try {
			runWorkflowSync(
					workflowOptions -> workflowOptions
							.setExecutionStartToCloseTimeout(Duration.ofSeconds(3))
							.setTaskList(taskList),
					() -> {
						runLocalActivitySync(
								options -> options,
								localActivityRunnableUid);
						//
					});
		} catch (RuntimeException ex) {
			//
			System.out.println();
		}
		System.out.println();
	}

	public static class RecursiveException extends RuntimeException {

		@Getter
		private final RecursiveObject recursiveObject;

		public RecursiveException(RecursiveObject recursiveObject) {
			this.recursiveObject = recursiveObject;
		}
	}

	@Getter
	public static class RecursiveObject {

		private final String foo = "foo";

		@Setter
		private RecursiveObject recursiveObject = null;
	}
}
