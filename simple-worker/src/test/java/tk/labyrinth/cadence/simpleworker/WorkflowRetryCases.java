package tk.labyrinth.cadence.simpleworker;

import com.uber.cadence.common.RetryOptions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import tk.labyrinth.cadence.misc4j.java.lang.ThreadUtils;
import tk.labyrinth.cadence.simpleworker.test.CadenceStudyTestBase;
import tk.labyrinth.cadence.simpleworker.test.ExecutionFunctionRegistry;
import tk.labyrinth.cadence.simpleworker.test.TestWorkerHandler;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Tag("cadence")
public class WorkflowRetryCases extends CadenceStudyTestBase {

	private static final String taskList = UUID.randomUUID().toString();

	@Test
	@Timeout(2)
	void testWorkflowRetryDueToException() {
		TestWorkerHandler.startNewFunctionalWorker(taskList);
		//
		List<Double> activityResults = Collections.synchronizedList(new ArrayList<>());
		//
		UUID activityTaskUid = ExecutionFunctionRegistry.putSupplier(System::currentTimeMillis);
		//
		runWorkflowSync(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(1))
						.setRetryOptions(new RetryOptions.Builder()
								.setExpiration(Duration.ofSeconds(2))
								.setInitialInterval(Duration.ofSeconds(1))
								.build())
						.setTaskList(taskList),
				() -> {
					Double activityResult = callGlobalActivitySync(
							options -> options.setScheduleToCloseTimeout(Duration.ofSeconds(1)),
							activityTaskUid);
					activityResults.add(activityResult);
					if (activityResults.size() == 1) {
						throw new RuntimeException("Ouch");
					}
				}
		);
		//
		Assertions.assertEquals(2, activityResults.size());
	}

	@Test
	@Timeout(4)
	void testWorkflowRetryDueToTimeout() {
		TestWorkerHandler.startNewFunctionalWorker(taskList);
		//
		List<Double> activityResults = Collections.synchronizedList(new ArrayList<>());
		//
		UUID activityTaskUid = ExecutionFunctionRegistry.putSupplier(System::currentTimeMillis);
		//
		Double result = callWorkflowSync(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(1))
						.setRetryOptions(new RetryOptions.Builder()
								.setExpiration(Duration.ofSeconds(3))
								.setInitialInterval(Duration.ofSeconds(1))
								.build())
						.setTaskList(taskList),
				() -> {
					Double activityResult = callGlobalActivitySync(
							options -> options.setScheduleToCloseTimeout(Duration.ofSeconds(1)),
							activityTaskUid);
					activityResults.add(activityResult);
					if (activityResults.size() == 1) {
						ThreadUtils.sleep(2000);
					}
					return activityResult;
				}
		);
		//
		Assertions.assertEquals(2, activityResults.size());
		Assertions.assertEquals(activityResults.get(1), result);
	}
}
