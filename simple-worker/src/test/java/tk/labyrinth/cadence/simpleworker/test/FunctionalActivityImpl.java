package tk.labyrinth.cadence.simpleworker.test;

import java.util.UUID;

public class FunctionalActivityImpl implements FunctionalActivity {

	@Override
	public Object run(UUID uid) {
		return ExecutionFunctionRegistry.get(uid).get();
	}
}
