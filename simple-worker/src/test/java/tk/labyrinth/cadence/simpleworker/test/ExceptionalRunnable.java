package tk.labyrinth.cadence.simpleworker.test;

public interface ExceptionalRunnable {

	void run() throws Exception;
}
