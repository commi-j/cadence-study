package tk.labyrinth.cadence.simpleworker.test;

import java.util.UUID;

public class FunctionalWorkflowImpl implements FunctionalWorkflow {

	@Override
	public Object run(UUID uid) {
		return ExecutionFunctionRegistry.get(uid).get();
	}
}
