package tk.labyrinth.cadence.simpleworker.test;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class ExecutionFunctionRegistry {

	private static final ConcurrentHashMap<UUID, Supplier<?>> suppliers = new ConcurrentHashMap<>();

	private static final ConcurrentHashMap<List<StackTraceElement>, UUID> stackTraceKeys = new ConcurrentHashMap<>();

	public static Supplier<?> get(UUID uid) {
		return suppliers.get(uid);
	}

	public static UUID putRunnable(Runnable runnable) {
		UUID uid = UUID.randomUUID();
		suppliers.put(uid, () -> {
			runnable.run();
			return null;
		});
		return uid;
	}

	public static UUID putRunnableWithStackTrace(Runnable runnable) {
		UUID uid = stackTraceKeys.computeIfAbsent(Arrays.stream(Thread.currentThread().getStackTrace())
				.collect(Collectors.toList()), key -> UUID.randomUUID());
		suppliers.put(uid, () -> {
			runnable.run();
			return null;
		});
		return uid;
	}

	public static UUID putSupplier(Supplier<?> supplier) {
		UUID uid = UUID.randomUUID();
		suppliers.put(uid, supplier);
		return uid;
	}
}
