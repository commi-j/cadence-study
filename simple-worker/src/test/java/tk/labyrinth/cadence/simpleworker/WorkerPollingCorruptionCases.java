package tk.labyrinth.cadence.simpleworker;

import com.uber.cadence.workflow.Workflow;
import org.junit.jupiter.api.Test;
import tk.labyrinth.cadence.misc4j.java.lang.ThreadUtils;
import tk.labyrinth.cadence.simpleworker.test.CadenceStudyTestBase;
import tk.labyrinth.cadence.simpleworker.test.TestWorkerHandler;

import java.time.Duration;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

class WorkerPollingCorruptionCases extends CadenceStudyTestBase {

	@Test
	void testFaultyLocalActivities() {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorker(
				workerOptions -> workerOptions);
		//
		AtomicInteger invocationCounter = new AtomicInteger(0);
		AtomicInteger completionCounter = new AtomicInteger(0);
		ConcurrentMap<String, AtomicInteger> executionsGroupedByWorkflowIds = new ConcurrentHashMap<>();
		//
		AtomicBoolean shouldRun = new AtomicBoolean(true);
		while (shouldRun.get()) {
			runWorkflowAsyncByCadence(
					options -> options
							.setExecutionStartToCloseTimeout(Duration.ofSeconds(300))
							.setTaskList(workerHandler.getTaskList())
							.setTaskStartToCloseTimeout(Duration.ofSeconds(1)),
					() -> {
						executionsGroupedByWorkflowIds
								.computeIfAbsent(
										Workflow.getWorkflowInfo().getWorkflowId(),
										key -> new AtomicInteger(0))
								.incrementAndGet();
						//
						runNonRepeatableLocalActivitySync(
								options -> options,
								() -> ThreadUtils.sleep(100));
						runNonRepeatableLocalActivitySync(
								options -> options,
								() -> {
									ThreadUtils.sleep(2000);
									throw new RuntimeException();
								});
					})
					.handle((result, fault) -> {
						completionCounter.incrementAndGet();
						//
						return fault != null ? fault : result;
					});
			invocationCounter.incrementAndGet();
			//
			System.out.println("INV = " + invocationCounter.get() + "\n" +
					"EXE = " + executionsGroupedByWorkflowIds.values().stream()
					.mapToInt(AtomicInteger::get)
					.sum() + "\n" +
					"UNI = " + executionsGroupedByWorkflowIds.size() + "\n" +
					"COM = " + completionCounter.get() + "\n");
			//
			ThreadUtils.sleep(100);
		}
	}

	@Test
	void testSleepyLocalActivities() {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorker(
				workerOptions -> workerOptions
						.setMaxConcurrentLocalActivityExecutionSize(2));
		//
		AtomicInteger invocationCounter = new AtomicInteger(0);
		AtomicInteger completionCounter = new AtomicInteger(0);
		ConcurrentMap<String, AtomicInteger> executionsGroupedByWorkflowIds = new ConcurrentHashMap<>();
		//
		AtomicBoolean shouldRun = new AtomicBoolean(true);
		while (shouldRun.get()) {
			runWorkflowAsyncByCadence(
					options -> options
							.setExecutionStartToCloseTimeout(Duration.ofSeconds(300))
							.setTaskList(workerHandler.getTaskList())
							.setTaskStartToCloseTimeout(Duration.ofSeconds(1)),
					() -> {
						executionsGroupedByWorkflowIds
								.computeIfAbsent(
										Workflow.getWorkflowInfo().getWorkflowId(),
										key -> new AtomicInteger(0))
								.incrementAndGet();
						//
						runLocalActivitySync(
								options -> options,
								() -> ThreadUtils.sleep(100));
						runLocalActivitySync(
								options -> options,
								() -> ThreadUtils.sleep(30_000));
					})
					.handle((result, fault) -> {
						completionCounter.incrementAndGet();
						//
						return fault != null ? fault : result;
					});
			invocationCounter.incrementAndGet();
			//
			System.out.println("INV = " + invocationCounter.get() + "\n" +
					"EXE = " + executionsGroupedByWorkflowIds.values().stream()
					.mapToInt(AtomicInteger::get)
					.sum() + "\n" +
					"UNI = " + executionsGroupedByWorkflowIds.size() + "\n" +
					"COM = " + completionCounter.get() + "\n");
			//
			ThreadUtils.sleep(100);
		}
	}

	// TODO: Why 23 - FJP?
	@Test
	void testSleepyWorkflows() {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorker(
				workerOptions -> workerOptions);
		//
		AtomicInteger invocationCounter = new AtomicInteger(0);
		AtomicInteger completionCounter = new AtomicInteger(0);
		ConcurrentMap<String, AtomicInteger> executionsGroupedByWorkflowIds = new ConcurrentHashMap<>();
		//
		AtomicBoolean shouldRun = new AtomicBoolean(true);
		while (shouldRun.get()) {
			runWorkflowAsyncByCadence(
					options -> options
							.setExecutionStartToCloseTimeout(Duration.ofSeconds(300))
							.setTaskList(workerHandler.getTaskList())
							.setTaskStartToCloseTimeout(Duration.ofSeconds(1)),
					() -> {
						executionsGroupedByWorkflowIds
								.computeIfAbsent(
										Workflow.getWorkflowInfo().getWorkflowId(),
										key -> new AtomicInteger(0))
								.incrementAndGet();
						//
						runNonRepeatableLocalActivitySync(
								options -> options,
								() -> ThreadUtils.sleep(100));
						runNonRepeatableLocalActivitySync(
								options -> options,
								() -> ThreadUtils.sleep(2000));
					})
					.handle((result, fault) -> {
						completionCounter.incrementAndGet();
						//
						return fault != null ? fault : result;
					});
			invocationCounter.incrementAndGet();
			//
			System.out.println("INV = " + invocationCounter.get() + "\n" +
					"EXE = " + executionsGroupedByWorkflowIds.values().stream()
					.mapToInt(AtomicInteger::get)
					.sum() + "\n" +
					"UNI = " + executionsGroupedByWorkflowIds.size() + "\n" +
					"COM = " + completionCounter.get() + "\n");
			//
			ThreadUtils.sleep(100);
		}
	}
}
