package tk.labyrinth.cadence.simpleworker.basic;

import org.junit.jupiter.api.Test;
import tk.labyrinth.cadence.simpleworker.test.CadenceStudyTestBase;
import tk.labyrinth.cadence.simpleworker.test.TestWorkerHandler;

import java.time.Duration;

public class BasicSignalCases extends CadenceStudyTestBase {

	@Test
	void testWaitForSignal() {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorker();
		//
		runWorkflowAsyncByFjp(
				workflowOptions -> workflowOptions
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(3))
						.setTaskList(workerHandler.getTaskList()),
				() -> {
					//
				}
		);
	}
}
