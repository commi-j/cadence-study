package tk.labyrinth.cadence.simpleworker;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import tk.labyrinth.cadence.misc4j.lib.junit5.ContribAssertions;
import tk.labyrinth.cadence.simpleworker.test.CadenceStudyTestBase;
import tk.labyrinth.cadence.simpleworker.test.TestWorkerHandler;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;

@Tag("cadence")
public class WorkflowWorkerRestartCase extends CadenceStudyTestBase {

	private final String actTaskList = UUID.randomUUID().toString();

	private final String wfTaskList = UUID.randomUUID().toString();

	/**
	 * It seems that Server detects shutdown within 5 seconds.
	 */
	@Test
	@Timeout(7)
	void test() {
		TestWorkerHandler.startNewFunctionalActivityWorker(actTaskList);
		//
		TestWorkerHandler wfWorkerHandler = TestWorkerHandler.startNewFunctionalWorkflowWorker(wfTaskList);
		//
		List<String> events = new CopyOnWriteArrayList<>();
		//
		UUID actRunnableUid = registerRunnable(() -> {
			events.add("ACTIVITY_STARTED");
			wfWorkerHandler.shutdownNow();
			TestWorkerHandler.startNewFunctionalWorkflowWorker(wfTaskList);
			events.add("ACTIVITY_COMPLETED");
		});
		UUID wfRunnableUid = registerRunnable(() -> {
			events.add("WORKFLOW_STARTED");
			try {
				runGlobalActivitySync(
						options -> options
								.setScheduleToCloseTimeout(Duration.ofSeconds(1))
								.setTaskList(actTaskList),
						actRunnableUid);
			} finally {
				// TODO: It seems that we actually does not leave this Thread first time despite 'shutdownNow' invocation.
				//  Potential resource leak, but who would call 'shutdownNow' in real environment?
				//
				events.add("WORKFLOW_COMPLETED");
			}
		});
		//
		Instant startedAt = Instant.now();
		runWorkflowSync(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(6))
						.setTaskList(wfTaskList),
				wfRunnableUid);
		//
		Assertions.assertEquals(
				List.of(
						//
						// Running WF first time.
						"WORKFLOW_STARTED",
						"ACTIVITY_STARTED",
						//
						// Shutting down first WF Worker, starting second one.
						"ACTIVITY_COMPLETED",
						//
						// Replaying WF with another Worker. We already have Activity result so no second invocation.
						"WORKFLOW_STARTED",
						"WORKFLOW_COMPLETED"),
				events);
		ContribAssertions.assertWithinRange(5000, 5500, Duration.between(startedAt, Instant.now()).toMillis());
	}
}
