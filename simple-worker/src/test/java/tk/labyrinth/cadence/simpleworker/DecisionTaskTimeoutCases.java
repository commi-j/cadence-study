package tk.labyrinth.cadence.simpleworker;

import com.uber.cadence.TimeoutType;
import com.uber.cadence.client.WorkflowOptions;
import com.uber.cadence.client.WorkflowTimedOutException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import tk.labyrinth.cadence.misc4j.java.lang.ThreadUtils;
import tk.labyrinth.cadence.misc4j.lib.junit5.ContribAssertions;
import tk.labyrinth.cadence.simpleworker.test.CadenceStudyTestBase;
import tk.labyrinth.cadence.simpleworker.test.LocalEventHistory;
import tk.labyrinth.cadence.simpleworker.test.TestWorkerHandler;

import java.time.Duration;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Tag("cadence")
public class DecisionTaskTimeoutCases extends CadenceStudyTestBase {

	private static final String taskList = UUID.randomUUID().toString();

	@Test
	@Timeout(3)
	void testLocalActivityLongerThanDecisionTaskTimeout() {
		TestWorkerHandler.startNewFunctionalWorker(taskList);
		//
		AtomicInteger workflowVisitCounter = new AtomicInteger(0);
		AtomicInteger localActivityVisitCounter = new AtomicInteger(0);
		//
		UUID localActivityRunnableUid = registerRunnable(() -> {
			int visitCount = localActivityVisitCounter.incrementAndGet();
			//
			if (visitCount == 1) {
				ThreadUtils.sleep(1200);
			}
		});
		//
		runWorkflowSync(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(2))
						.setTaskList(taskList)
						.setTaskStartToCloseTimeout(Duration.ofSeconds(1))
				,
				() -> {
					workflowVisitCounter.incrementAndGet();
					//
					runLocalActivitySync(
							options -> options,
							localActivityRunnableUid);
				});
		//
		Assertions.assertEquals(2, workflowVisitCounter.get());
		Assertions.assertEquals(2, localActivityVisitCounter.get());
		// TODO: Assert time
	}

	@Test
	@Timeout(3)
	void testLocalActivityShorterThanDecisionTaskTimeout() {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorker(taskList);
		//
		AtomicInteger workflowVisitCounter = new AtomicInteger(0);
		AtomicInteger localActivityVisitCounter = new AtomicInteger(0);
		//
		UUID localActivityRunnableUid = registerRunnable(localActivityVisitCounter::incrementAndGet);
		//
		runWorkflowSync(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(2))
						.setTaskList(taskList)
						.setTaskStartToCloseTimeout(Duration.ofSeconds(1))
				,
				() -> {
					workflowVisitCounter.incrementAndGet();
					//
					runLocalActivitySync(
							options -> options,
							localActivityRunnableUid);
				});
		//
		Assertions.assertEquals(1, workflowVisitCounter.get());
		Assertions.assertEquals(1, localActivityVisitCounter.get());
		// TODO: Assert time
	}

	/**
	 * Unsolvable repetitive exception:
	 * 'java.lang.IllegalStateException: ReplayDecider expects next event id at X. History's previous started event id is X-3'.
	 */
	@Test
	@Timeout(3)
	void testTimeoutInLocalActivityAfterLocalActivity() {
		TestWorkerHandler.startNewFunctionalWorker(taskList);
		//
		LocalEventHistory localEventHistory = new LocalEventHistory();
		//
		UUID firstActivityRunnableUid = registerRunnable(() -> localEventHistory.addEntry("FIRST_ACTIVITY_START"));
		UUID secondActivityRunnableUid = registerRunnable(() -> {
			int visitCount = localEventHistory.addEntry("SECOND_ACTIVITY_START");
			//
			if (visitCount == 1) {
				localEventHistory.addEntry("BEFORE_SLEEP");
				ThreadUtils.sleep(1200);
			} else {
				localEventHistory.addEntry("AVOID_SLEEP");
			}
		});
		//
		ContribAssertions.assertThrows(
				() -> runWorkflowSync(
						options -> options
								.setExecutionStartToCloseTimeout(Duration.ofSeconds(2))
								.setTaskList(taskList)
								.setTaskStartToCloseTimeout(Duration.ofSeconds(1))
						,
						() -> {
							localEventHistory.addEntry("WORKFLOW_START");
							//
							runLocalActivitySync(
									options -> options,
									firstActivityRunnableUid);
							runLocalActivitySync(
									options -> options,
									secondActivityRunnableUid);
							//
						}),
				fault -> {
					Assertions.assertEquals(WorkflowTimedOutException.class, fault.getClass());
					Assertions.assertEquals(
							TimeoutType.START_TO_CLOSE,
							((WorkflowTimedOutException) fault).getTimeoutType());
				});
		//
		ContribAssertions.assertEquals(
				List.of(
						"WORKFLOW_START",
						"FIRST_ACTIVITY_START",
						"SECOND_ACTIVITY_START",
						"BEFORE_SLEEP",
						"WORKFLOW_START",
						"SECOND_ACTIVITY_START",
						"AVOID_SLEEP",
						"WORKFLOW_START",
						"SECOND_ACTIVITY_START",
						"AVOID_SLEEP"),
				localEventHistory.getEntries().stream().limit(10));
		// TODO: Assert time between 1200 & 1800.
	}

	@Test
	@Timeout(3)
	void testTimeoutInWorkflow() {
		TestWorkerHandler.startNewFunctionalWorker(taskList);
		//
		LocalEventHistory localEventHistory = new LocalEventHistory();
		//
		runWorkflowSync(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(2))
						.setTaskList(taskList)
						.setTaskStartToCloseTimeout(Duration.ofSeconds(1))
				,
				() -> {
					int visitCount = localEventHistory.addEntry("WORKFLOW_START");
					//
					if (visitCount == 1) {
						localEventHistory.addEntry("BEFORE_SLEEP");
						ThreadUtils.sleep(1200);
					} else {
						localEventHistory.addEntry("AVOID_SLEEP");
					}
				});
		//
		Assertions.assertEquals(
				List.of(
						"WORKFLOW_START",
						"BEFORE_SLEEP",
						"WORKFLOW_START",
						"AVOID_SLEEP"),
				localEventHistory.getEntries());
		// TODO: Assert time between 1200 & 1800.
	}

	/**
	 * Suspicious that LACT is executed twice - looks like we don't report right after completion but some time later.
	 */
	@Test
	@Timeout(3)
	void testTimeoutInWorkflowAfterLocalActivity() {
		TestWorkerHandler.startNewFunctionalWorker(taskList);
		//
		LocalEventHistory localEventHistory = new LocalEventHistory();
		//
		UUID activityRunnableUid = registerRunnable(() -> localEventHistory.addEntry("ACTIVITY_START"));
		//
		runWorkflowSync(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(2))
						.setTaskList(taskList)
						.setTaskStartToCloseTimeout(Duration.ofSeconds(1))
				,
				() -> {
					int visitCount = localEventHistory.addEntry("WORKFLOW_START");
					//
					runLocalActivitySync(
							options -> options,
							activityRunnableUid);
					//
					if (visitCount == 1) {
						localEventHistory.addEntry("BEFORE_SLEEP");
						ThreadUtils.sleep(1200);
					} else {
						localEventHistory.addEntry("AVOID_SLEEP");
					}
				});
		//
		Assertions.assertEquals(
				List.of(
						"WORKFLOW_START",
						"ACTIVITY_START",
						"BEFORE_SLEEP",
						"WORKFLOW_START",
						"ACTIVITY_START",
						"AVOID_SLEEP"),
				localEventHistory.getEntries());
		// TODO: Assert time between 1200 & 1800.
	}

	/**
	 * Unsolvable repetitive exception:
	 * 'java.lang.IllegalStateException: ReplayDecider expects next event id at X. History's previous started event id is X-3'.
	 */
	@Test
	@Timeout(3)
	void testTimeoutInWorkflowAfterTwoLocalActivities() {
		TestWorkerHandler.startNewFunctionalWorker(taskList);
		//
		LocalEventHistory localEventHistory = new LocalEventHistory();
		//
		UUID firstActivityRunnableUid = registerRunnable(() -> localEventHistory.addEntry("FIRST_ACTIVITY_START"));
		UUID secondActivityRunnableUid = registerRunnable(() -> localEventHistory.addEntry("SECOND_ACTIVITY_START"));
		//
		ContribAssertions.assertThrows(
				() -> runWorkflowSync(
						options -> options
								.setExecutionStartToCloseTimeout(Duration.ofSeconds(2))
								.setTaskList(taskList)
								.setTaskStartToCloseTimeout(Duration.ofSeconds(1))
						,
						() -> {
							int visitCount = localEventHistory.addEntry("WORKFLOW_START");
							//
							runLocalActivitySync(
									options -> options,
									firstActivityRunnableUid);
							runLocalActivitySync(
									options -> options,
									secondActivityRunnableUid);
							//
							if (visitCount == 1) {
								localEventHistory.addEntry("BEFORE_SLEEP");
								ThreadUtils.sleep(1200);
							} else {
								localEventHistory.addEntry("AVOID_SLEEP");
							}
						}),
				fault -> {
					Assertions.assertEquals(WorkflowTimedOutException.class, fault.getClass());
					Assertions.assertEquals(
							TimeoutType.START_TO_CLOSE,
							((WorkflowTimedOutException) fault).getTimeoutType());
				});
		//
		ContribAssertions.assertEquals(
				List.of(
						"WORKFLOW_START",
						"FIRST_ACTIVITY_START",
						"SECOND_ACTIVITY_START",
						"BEFORE_SLEEP",
						"WORKFLOW_START",
						"SECOND_ACTIVITY_START",
						"WORKFLOW_START",
						"SECOND_ACTIVITY_START"),
				localEventHistory.getEntries().stream().limit(8));
		// TODO: Assert time between 1200 & 1800.
	}

	@Test
	void testTooLargeDecisionTaskTimeout() {
		ContribAssertions.assertThrows(
				() -> new WorkflowOptions.Builder()
						.setTaskStartToCloseTimeout(Duration.ofSeconds(61)),
				fault -> Assertions.assertEquals(
						"java.lang.IllegalArgumentException: TaskStartToCloseTimeout over one minute: PT1M1S",
						fault.toString()));
	}

	@Test
	@Timeout(3)
	void testTwoLocalActivitiesLongerThanDecisionTaskTimeout() {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorker(taskList);
		//
		AtomicInteger workflowVisitCounter = new AtomicInteger(0);
		AtomicInteger firstLocalActivityVisitCounter = new AtomicInteger(0);
		AtomicInteger secondLocalActivityVisitCounter = new AtomicInteger(0);
		//
		UUID firstLocalActivityRunnableUid = registerRunnable(() -> {
			int visitCount = firstLocalActivityVisitCounter.incrementAndGet();
			//
			if (visitCount == 1) {
				ThreadUtils.sleep(600);
			}
		});
		UUID secondLocalActivityRunnableUid = registerRunnable(() -> {
			int visitCount = secondLocalActivityVisitCounter.incrementAndGet();
			//
			if (visitCount == 1) {
				ThreadUtils.sleep(600);
			}
		});
		//
		runWorkflowSync(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(2))
						.setTaskList(taskList)
						.setTaskStartToCloseTimeout(Duration.ofSeconds(1))
				,
				() -> {
					workflowVisitCounter.incrementAndGet();
					//
					runLocalActivitySync(
							options -> options,
							firstLocalActivityRunnableUid);
					runLocalActivitySync(
							options -> options,
							secondLocalActivityRunnableUid);
				});
		//
		Assertions.assertEquals(1, workflowVisitCounter.get());
		Assertions.assertEquals(1, firstLocalActivityVisitCounter.get());
		Assertions.assertEquals(1, secondLocalActivityVisitCounter.get());
		// TODO: Assert time
	}
}
