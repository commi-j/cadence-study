package tk.labyrinth.cadence.test0;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import tk.labyrinth.cadence.simpleworker.test.CadenceStudyTestBase;
import tk.labyrinth.cadence.simpleworker.test.TestWorkerHandler;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class WorkflowReplayCases extends CadenceStudyTestBase {

	@Test
	@Timeout(2)
	void testSimpleWorkflowReplays() {
		TestWorkerHandler workerHandler = TestWorkerHandler.startNewFunctionalWorkerWithCustomFactoryOptions(
				options -> options
						.setDisableStickyExecution(true));
		//
		List<String> events = new CopyOnWriteArrayList<>();
		//
		runWorkflowSync(
				options -> options
						.setExecutionStartToCloseTimeout(Duration.ofSeconds(2))
						.setTaskList(workerHandler.getTaskList()),
				() -> {
					try {
						events.add("WORKFLOW_STARTED");
						//
						runNonRepeatableGlobalActivitySync(
								options -> options
										.setScheduleToCloseTimeout(Duration.ofSeconds(1))
										.setTaskList(workerHandler.getTaskList()),
								() -> events.add("ACTIVITY_0"));
						//
						events.add("WORKFLOW_BETWEEN_ACTIVITIES");
						//
						runNonRepeatableGlobalActivitySync(
								options -> options
										.setScheduleToCloseTimeout(Duration.ofSeconds(1))
										.setTaskList(workerHandler.getTaskList()),
								() -> events.add("ACTIVITY_1"));
						//
						events.add("WORKFLOW_COMPLETED");
					} finally {
						events.add("WORKFLOW_FINALLY");
					}
				}
		);
		//
		Assertions.assertEquals(
				List.of(
						"WORKFLOW_STARTED",
						"WORKFLOW_FINALLY",
						"ACTIVITY_0",
						"WORKFLOW_STARTED",
						"WORKFLOW_BETWEEN_ACTIVITIES",
						"WORKFLOW_FINALLY",
						"ACTIVITY_1",
						"WORKFLOW_STARTED",
						"WORKFLOW_BETWEEN_ACTIVITIES",
						"WORKFLOW_COMPLETED",
						"WORKFLOW_FINALLY"),
				events);
	}
}
