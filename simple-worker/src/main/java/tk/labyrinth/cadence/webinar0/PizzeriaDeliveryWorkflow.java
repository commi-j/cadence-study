package tk.labyrinth.cadence.webinar0;

import com.uber.cadence.workflow.WorkflowMethod;
import tk.labyrinth.cadence.webinar0.model.PizzeriaOrderRequest;

public interface PizzeriaDeliveryWorkflow {

	@WorkflowMethod
	void makeOrder(PizzeriaOrderRequest request);
}
