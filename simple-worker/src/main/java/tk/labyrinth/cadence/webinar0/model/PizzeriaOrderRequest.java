package tk.labyrinth.cadence.webinar0.model;

import lombok.Value;

@Value
public class PizzeriaOrderRequest {

	Object address;

	PizzeriaBasket basket;

	String paymentData;

	String paymentType;
}
