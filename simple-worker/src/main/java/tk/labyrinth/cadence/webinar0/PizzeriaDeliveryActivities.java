package tk.labyrinth.cadence.webinar0;

import com.uber.cadence.activity.ActivityMethod;
import tk.labyrinth.cadence.webinar0.model.ComposedOrder;
import tk.labyrinth.cadence.webinar0.model.PaymentOutcome;
import tk.labyrinth.cadence.webinar0.model.PizzeriaBasket;
import tk.labyrinth.cadence.webinar0.model.ReservationOutcome;

import java.util.UUID;

public interface PizzeriaDeliveryActivities {

	@ActivityMethod
	ComposedOrder cookAndCompose(UUID reservationUid);

	@ActivityMethod
	ReservationOutcome deliver(ComposedOrder composedOrder, Object address);

	@ActivityMethod
	ReservationOutcome getCash();

	@ActivityMethod
	PaymentOutcome processPayment(String paymentData);

	@ActivityMethod
	ReservationOutcome reserveIngredients(PizzeriaBasket basket);

	@ActivityMethod
	ReservationOutcome rollbackPayment(UUID paymentUid);

	@ActivityMethod
	ReservationOutcome rollbackReservation(UUID reservationUid);
}
