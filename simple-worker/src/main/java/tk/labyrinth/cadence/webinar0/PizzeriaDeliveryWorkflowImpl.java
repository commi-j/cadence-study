package tk.labyrinth.cadence.webinar0;

import com.uber.cadence.workflow.Saga;
import lombok.RequiredArgsConstructor;
import tk.labyrinth.cadence.webinar0.model.ComposedOrder;
import tk.labyrinth.cadence.webinar0.model.PaymentOutcome;
import tk.labyrinth.cadence.webinar0.model.PizzeriaOrderRequest;
import tk.labyrinth.cadence.webinar0.model.ReservationOutcome;

import java.util.Objects;

@RequiredArgsConstructor
public class PizzeriaDeliveryWorkflowImpl implements PizzeriaDeliveryWorkflow {

	private final PizzeriaDeliveryActivities activities;

	@Override
	public void makeOrder(PizzeriaOrderRequest request) {
		Saga saga = new Saga(new Saga.Options.Builder().build());
		try {
			ReservationOutcome reservationOutcome = activities.reserveIngredients(request.getBasket());
			saga.addCompensation(activities::rollbackReservation, reservationOutcome.getUid());
			//
			PaymentOutcome paymentOutcome;
			if (Objects.equals(request.getPaymentType(), "card")) {
				paymentOutcome = activities.processPayment(request.getPaymentData());
				saga.addCompensation(activities::rollbackPayment, paymentOutcome.getUid());
			} else {
				paymentOutcome = null;
			}
			//
			ComposedOrder composedOrder = activities.cookAndCompose(reservationOutcome.getUid());
			//
			activities.deliver(composedOrder, request.getAddress());
			//
			if (paymentOutcome == null) {
				activities.getCash();
			}
		} catch (RuntimeException ex) {
			saga.compensate();
			throw ex;
		}
	}
}
