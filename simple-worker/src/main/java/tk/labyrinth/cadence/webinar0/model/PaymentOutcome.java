package tk.labyrinth.cadence.webinar0.model;

import lombok.Value;

import java.util.UUID;

@Value
public class PaymentOutcome {

	UUID uid;
}
