package tk.labyrinth.cadence.tools;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uber.cadence.converter.DataConverter;
import com.uber.cadence.converter.DataConverterException;
import com.uber.cadence.converter.JsonDataConverter;

import java.lang.reflect.Type;

/**
 * @see JsonDataConverter
 */
public abstract class JacksonDataConverter implements DataConverter {

	private static final Object[] EMPTY_OBJECT_ARRAY = new Object[0];

	private static final String TYPE_FIELD_NAME = "type";

	private static final String JSON_CONVERTER_TYPE = "JSON";

	private static final String CLASS_NAME_FIELD_NAME = "className";

	private final ObjectMapper objectMapper = new ObjectMapper();

	@Override
	public <T> T fromData(byte[] content, Class<T> valueClass, Type valueType) throws DataConverterException {
		// TODO: Implement.
		throw new RuntimeException();
	}
//	@Override
//	public Object[] fromDataArray(byte[] content, Type... valueTypes) throws DataConverterException {
//		try {
//			if (content == null) {
//				if (valueTypes.length == 0) {
//					return EMPTY_OBJECT_ARRAY;
//				}
//				throw new DataConverterException(
//						"Content doesn't match expected arguments", content, valueTypes);
//			}
//			if (valueTypes.length == 1) {
//				Object result = gson.fromJson(new String(content, StandardCharsets.UTF_8), valueTypes[0]);
//				return new Object[]{result};
//			}
//			JsonElement element = JsonParser.parseString(new String(content, StandardCharsets.UTF_8));
//			JsonArray array;
//			if (element instanceof JsonArray) {
//				array = element.getAsJsonArray();
//			} else {
//				array = new JsonArray();
//				array.add(element);
//			}
//			Object[] result = new Object[valueTypes.length];
//			for (int i = 0; i < valueTypes.length; i++) {
//				if (i >= array.size()) { // Missing arugments => add defaults
//					Type t = valueTypes[i];
//					if (t instanceof Class) {
//						result[i] = Defaults.defaultValue((Class<?>) t);
//					} else {
//						result[i] = null;
//					}
//				} else {
//					objectMapper.readValue(array.get(i), objectMapper.constructType(valueTypes[i]));
//					result[i] = gson.fromJson(array.get(i), valueTypes[i]);
//				}
//			}
//			return result;
//		} catch (DataConverterException e) {
//			throw e;
//		} catch (Exception e) {
//			throw new DataConverterException(content, valueTypes, e);
//		}
//	}

	@Override
	public byte[] toData(Object... value) throws DataConverterException {
		// TODO: Implement.
		throw new RuntimeException();
	}
}
