package tk.labyrinth.cadence.tools;

import com.uber.cadence.converter.DataConverter;
import com.uber.cadence.converter.DataConverterException;
import com.uber.cadence.converter.JsonDataConverter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.lang.reflect.Type;
import java.util.function.BiFunction;
import java.util.function.Function;

@Accessors(fluent = true)
public class FunctionalGsonDataConverter implements DataConverter {

	private final DataConverter gsonDataConverter = JsonDataConverter.getInstance();

	/**
	 * First parameter - superCallback;<br>
	 * Second parameter - method input values;<br>
	 * Output - method output;<br>
	 */
	@Setter
	private BiFunction<Function<Object[], byte[]>, Object[], byte[]> toDataFunction = null;

	@Override
	public <T> T fromData(byte[] content, Class<T> valueClass, Type valueType) throws DataConverterException {
		return gsonDataConverter.fromData(content, valueClass, valueType);
	}

	@Override
	public Object[] fromDataArray(byte[] content, Type... valueTypes) throws DataConverterException {
		return gsonDataConverter.fromDataArray(content, valueTypes);
	}

	@Override
	public byte[] toData(Object... values) throws DataConverterException {
		return toDataFunction != null
				? toDataFunction.apply(gsonDataConverter::toData, values)
				: gsonDataConverter.toData(values);
	}
}
