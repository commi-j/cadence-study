package tk.labyrinth.cadence.misc4j.java.util.concurrent;

import java.util.concurrent.CountDownLatch;

/**
 * @author Commitman
 * @version 1.0.0
 */
public class CountDownLatchUtils {

	/**
	 * @param countDownLatch non-null
	 *
	 * @since 1.0.0
	 */
	public static void await(CountDownLatch countDownLatch) {
		try {
			countDownLatch.await();
		} catch (InterruptedException ex) {
			throw new RuntimeException(ex);
		}
	}
}
